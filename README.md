# ELSI - ELectronic Structure Infrastructure (development)

## About

ELSI provides and enhances open-source software packages which solve or
circumvent eigenvalue problems in self-consistent field calculations based on
the Kohn-Sham density-functional theory. For more information, please visit the
[ELSI interchange](https://elsi-interchange.org) website.

## Installation

The standard installation of ELSI requires:

* CMake (3.0 or newer)
* Fortran compiler (Fortran 2003)
* C compiler (C99)
* C++ compiler (C++11, optional)
* MPI (MPI-3)
* BLAS, LAPACK, ScaLAPACK
* CUDA (optional)

Installation with recent versions of Cray, GNU, IBM, Intel, and PGI compilers
has been tested. For a complete description of the installation process, please
refer to [`./INSTALL.md`](./INSTALL.md).

## More

A User's Guide is available at [`./doc/elsi_manual.pdf`](./doc/elsi_manual.pdf).
For comments, feedback, and suggestions, please
[contact the ELSI team](mailto:elsi-team@duke.edu).

Copyright (c) 2015-2020, the ELSI team. All rights reserved.
